//
//  CollectionViewController.swift
//  goProInstagram
//
//  Created by Amy Acuff on 7/16/16.
//  Copyright © 2016 Winnning Edge Apps. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

typealias DictPair = [String: AnyObject]
private let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
private let numberOfItemsPerRow = 2


class CollectionViewController: UICollectionViewController {
    
    private let reuseIdentifier = "Cell"
    private var resultsArray = [InstagramItem]()
    private let service = Service()
    private var wantComments: Bool = false
    
    var aPlayer = AVPlayer()
    let moviePlayerController = AVPlayerViewController()
    var isPlaying: Bool = false
   
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var likesCommentsSwitch: UISegmentedControl!
   
    

    override func viewDidLoad() {
        super.viewDidLoad()

      // self.collectionView!.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier:reuseIdentifier)
        
        // Do any additional setup after loading the view.
        service.fetchItems() {
            results, error in
            
            self.activityIndicator.stopAnimating()
            
            if error != nil {
                print("Error fetching : \(error)")
            }
            
            
            
            if results.count > 0 {
                
                print("Found \(results.count) items")
                
                for item in results{
                    guard let dictionary = item as? Dictionary<String, AnyObject>,
                    let type = dictionary["type"] as? String,
                    let images = dictionary["images"] as? DictPair,
                    let lowResolution = images["low_resolution"] as? DictPair,
                    let url = lowResolution["url"] as? String,
                    let likes = dictionary["likes"] as? DictPair,
                    let likeCount = likes["count"] as? Int,
                    let comments = dictionary["comments"] as? DictPair,
                    let commentCount = comments["count"] as? Int
        
                    
                    else {
                      
                       continue
                    }
                    
                    
                 
                    
                    var myVideoURL: String = ""
                    if let videos = dictionary["videos"] as? DictPair{
                        if let lowResolutionVideo = videos["low_resolution"] as? DictPair{
                            if let videoURL: String = lowResolutionVideo["url"] as? String{
                                myVideoURL = videoURL
                            }else{
                                myVideoURL = ""
                            }
                        }else{
                            
                        }
                    }else{
                        
                    }
                    
                    
                    
                    let myItem = InstagramItem(type:type, commentCount:commentCount, likeCount: likeCount, url : url, videoURL: myVideoURL)
                     print("myItem: \(myItem)")
                    self.resultsArray.append(myItem)
                    
                }
                
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.collectionView!.reloadData()
                    self.activityIndicator.stopAnimating()
                }
                
                
            }else{
                print("No items in feed")
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    
    @IBAction func sortBy(sender: AnyObject) {
        
        if(sender.selectedSegmentIndex == 0){
            //sort by likes
            wantComments = false;
            
        }else{
            //sort by comments
             wantComments = true;
        }
        
       
        
        dispatch_async(dispatch_get_main_queue()) {
            self.collectionView!.reloadData()
        }
    }

   
    // MARK: UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
       
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return self.resultsArray.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
         print("cellForRow")
        
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CollectionViewCell
        
    
    
        // Configure the cell
        let instagramItem: InstagramItem
        
        instagramItem = self.resultsArray[indexPath.row]
        
        
       
        
        if let url  = NSURL(string: instagramItem.url),
            let data = NSData(contentsOfURL: url)
        {
            
            
            cell.imageView.image = UIImage(data: data)
            
            cell.imageView.contentMode = .ScaleAspectFill
        
            
            cell.layer.shouldRasterize = true
            cell.layer.rasterizationScale = UIScreen.mainScreen().scale
       
        
        if(instagramItem.type == "video"){
            print("we have a video")
             cell.playImage.hidden = false
            
            }else{
            cell.playImage.hidden = true
        }
            
        }
        
        
        if(wantComments){
            cell.myLabel.text = String(instagramItem.commentCount)
        }else{
            cell.myLabel.text = String(instagramItem.likeCount)
        }
        
        
    
        return cell
    }

    // MARK: UICollectionViewDelegate
    
    
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        
        print("did select item")
        
        
        if(isPlaying){
            removeVideoPlayer()
        }
        addVideoPlayerAt(indexPath)
        
        
        
    }
    
    override func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath){
        
        if collectionView.indexPathsForVisibleItems().indexOf(indexPath) != nil {
            print("Cell still visible")
        } else {
            print("Cell at this indexPath is no longer visible")
            
            
            if(isPlaying){
                removeVideoPlayer()
            }
            
        }
        
        
    }

    
     // MARK: Video Player
    
    func removeVideoPlayer(){
        
        aPlayer.pause()
        moviePlayerController.view.removeFromSuperview()
        
        self.isPlaying = false
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
     func addVideoPlayerAt(indexPath:NSIndexPath){
        let cell : UICollectionViewCell = collectionView!.cellForItemAtIndexPath(indexPath)!
        
        let instagramItem: InstagramItem
        
        instagramItem = self.resultsArray[indexPath.row]
        
        let fileUrl = NSURL(string: instagramItem.videoURL!)
        
        
        aPlayer = AVPlayer(URL: fileUrl!)
        
        moviePlayerController.player = aPlayer
        moviePlayerController.view.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)
        moviePlayerController.videoGravity = AVLayerVideoGravityResizeAspectFill
        moviePlayerController.view.sizeToFit()
        moviePlayerController.showsPlaybackControls = false
        cell.addSubview(moviePlayerController.view)
        aPlayer.play()
        
        self.isPlaying = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CollectionViewController.playerDidFinishPlaying(_:)),
                                                         name: AVPlayerItemDidPlayToEndTimeNotification, object: aPlayer.currentItem)

        
    }
    
    
    
    
    
    func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        
        removeVideoPlayer()
       
      
    }
    
    
    
  
   
}

extension CollectionViewController : UICollectionViewDelegateFlowLayout {
    
   
   
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
         print("collectionViewLayout")
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: size)
    }
    
   
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}




