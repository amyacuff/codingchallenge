//
//  InstagramItem.swift
//  goProInstagram
//
//  Created by Amy Acuff on 7/16/16.
//  Copyright © 2016 Winnning Edge Apps. All rights reserved.
//

import UIKit

struct InstagramItem {
    let type: String
    let commentCount: Int
    let likeCount: Int
    let url : String
    let videoURL: String?
    init (type: String, commentCount:Int, likeCount: Int, url : String, videoURL: String?) {         self.type = type
        self.commentCount = commentCount
        self.likeCount = likeCount
        self.url = url
        self.videoURL = videoURL
        
    }

}



